

# Carro multifuncional

Con esta guía puedes ensamblar el carro multifuncional, un kit para aprender a programar una gran variedad de proyectos afines como el *seguidor de línea*, *esquiva obstáculos*, *recorre laberintos*,
*sigue luz*, *sigue sonido* y todo lo que puedas imaginar.

Para realizar esta gía colaboraron

* Gerardo Iván González Rosales
* José Augusto Molina Vieira
* Erick Martínez González
* Bryan Daniel Beatriz Durán
* Dámaris Beatriz
* Pedro Fuentes
* Pavel E. Vázquez Mtz. pavel_e@riseup.net

# Manos a la obra

Cuando te entreguen el kit debes recibir lo siguiente:

* 1 bolsa transparente con diversos componentes.
* 1 bolsa antiestéticas con shield para motores.
* 1 bolsa antiestática con Arduino UNO.
* 2 cables rojos.
* 2 cables negros.
* 1 cable USB.

Es importante que consideres las siguientes herramientas para armar tu kit:

* Desarmador pequeño de cruz.
* Pinzas de punta para electrónica.
* Pinzas de corte para electrónica.
* Cautin y soldadura.

# Lista de materiales
Revisemos que cuentas con todos los materiales necesarios:

## Materiales contenidos en la bolsa grande

En la bolsa grande encontrarás los siguientes materiales:

* Base amarilla de acrílico.
* 1 Porta batería para pilas AA.
* 2 Llantas para motorreductor.
* 2 Motorreductor recto de doble espiga.
* 1 Rueda loca.

La bolsa grande contiene una bolsa pequeña con los siguientes materiales:

  * 4 Tubos de plástico (Popotitos).
  * 8 Tornillos pequeños.
  * 4 Tuercas de latón para chasis.
  * 4 Tornillos 1 1/8 " * 1/8" con cabeza cónicas.
  * 4 Tornillos 3/4" * 1/16" con tuerca
  * 4 Tornillos 1/4" * 1/16" con tuerca
  * 4 sujetadores para motorreductor

## Materiales Sueltos

* 1 Arduino uno.
* 1 Shield para motores.
* 2 Cables rojos de 15cm.
* 2 Cable negros de 15cm.
* 1 Cable USB.

# Ensamble

A continuacion describimos (al menos eso intentamos) una propuesta sobre cómo debe ensamblarse el *Carro Mutifuncional* en su modo básico, es decir sin sensores.

Abre primero la bolsa que contiene la placa amarilla y verifica que tienes todos los elementos que se mencionan en la lista de materiales.

También saca de sus bolsas antiestáticas el Arduino UNO y el Shield para motores.


## Preparación del chasis

1. Toma la base amarilla de acritlico, es la más grande, y remueve la película protectora de ambos lados.

1. Saca el Arduino UNO de su bolsa antiestática y busca los cuatro orificios del chasis que coinciden con los cuatro orificios de la placa. **Pista: Si no lo encuentras por un lado prueba por el otro.**

1. Coloca entre el acrílico y el arduino las tuercas de latón y fíjalas con los tornillos pequeños uno por arriba y uno por abajo.

## Preparación de motores

1. Debes soldar a cada motorreductor un cable rojo y un cable negro. Para ello haz uso del cautín y la soldadura.

1. Coloca los moto reductores  con los 4 sujetadores, para ello:

	1. Introduce un sujetador en el acrílico y el otro sujetador en la pestaña
  que está en la orilla

	1. Entre los sujetadores coloca un motorreductor.

	1. Fija el motorreductor con 2 tornillos cónicos. **Nota: cuida que la cabeza de los tornillos cónicos quede hacia el exterior**

	1. Los motoreductores deben quedar del lado contrario donde está el Arduino. **Nota: verifica que los conectores del moto reductor queden hacia afuera.**

1. Coloca las llantas en la espiga del motorreductor.

1. En la parte de abajo coloca la base para baterías entre los dos motores, utiliza dos tornillos largos de 1/16".

1. Coloca la rueda loca enfrente de la base para baterías, Utiliza para ello los dos tornillos pequeños con tuerca. **Nota: cuida que la rueda loca no choque con la base para pilas**

## Preparación de las Conexiones

Ahora hay que realizar algunas conexiones antes de finalizar.

1. Coloca sobre el arduino el Shield para motores. **Nota: Solo hay una forma correcta de hacerlo.**

1. Haz un nudo en los cables de cada motor.

1. Pasa los cables por el orificio más cercano hacia el otro lado de la base de acrílico.

1. Busca en el shield las etiquetas que identifican a los conectores para los motores M1 y M2.

1. Fija los cables rojos de cada motor en los extremos y los cables negros en la parte interna del conector.

# A programar!!!

Ahora es momento de probar la programacion para verificar que todo funciona correctamente.

# Por hacer...

* Escribir programa para verificar el funcionamiento del Carro Multifuncional.
* Agregar fotografías para ilustrar el proceso de ensamblado.
