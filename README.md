# Carro Multifuncional

Este repositorio tiene como objetivo facilitar el proceso de  ensamble, uso y programación del Kit de Robótica al que hemos llamado **Carro Multifuncional**.


* Si tienes un kit sin ensamblar tal vez quieras consultar la [Propuesta de ensamble](propuestaEnsamble/propuestaEnsamble.md)

* Si tienes un Kit ya ensamblado y quieres verificar su funcionamiento puedes seguir la guía [Test Carro Multifuncional](proyectos/00test/testCarroMultifuncional.md)

**Recuerda que *git* es un lugar donde puedes descargar, consultar y conrtribuir asi que tus aportaciones y consultas son bienvenidas**
