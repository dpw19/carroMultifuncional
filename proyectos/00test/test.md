Aquí se encuentra el código para que verifiques que el carro multifuncional esta ensamblado correctamente y que el Arduino se puede programar.

# ¿Qué debe suceder?

Tu carro debe hacer lo siguiente:
  * Avanzar
  * Retroceder
  * Girar a la derecha
  * Girar a la izquierda

# ¿Y si no lo hace?
Cambia la polaridad de los motores que están conectados al shield de motores. 

## Código

```
/*
 *
 * Carro Multifuncional Programa de Prueba
 *
 * Este programa genera una secuencia determinada para verificar que
 * las conexiones de tu carro multifuncional son correctas
 *
 * Tu carro debe hacer las siguientes funciones
 * 
 *  * Avanzar
 *  * Detenerse
 *  * Girar a la derecha
 *  * Detenerse
 *  * Retroceder
 *  * Detenerse
 *  * Girar a la izquierda
 *  * Detenerse
 *  
 *  Si tienes sugerencias o aportes puedes hacerlos en 
 *  gitlab.com/dpw19/carroMultifuncional
 *  
 *  Author: Pavel E. Vázquez Mtz. pavel_e@riseup.net
 *  
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *    
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

#include <AFMotor.h>

AF_DCMotor motorDerecho(1);
AF_DCMotor motorIzquierdo(2);

void setup() {
  Serial.begin(19200);
  Serial.println("Carro Multifuncional");
}

void loop() {
  uint8_t i;

  adelante(3);
  detener(2);
  
  derecha(20);
  detener(2);

  atras(3);
  detener(2);
  
  izquierda(20);
  detener(2);

  Serial.println("Hecho!!!");

  while(1);
}


void adelante(int a){
  uint8_t i;

  Serial.println("Adelante...");

  motorDerecho.run(FORWARD);
  motorIzquierdo.run(FORWARD);

  for (i=0; i < 255; i++) {
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }
  delay(500*a);
}

void detener(int a){
  uint8_t i;

  Serial.println("Detener...");

  for (i=255; i != 0 ; i--) {
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);  
    delay(5);
  }
  motorDerecho.run(RELEASE);
  motorIzquierdo.run(RELEASE);
  delay(500*a);
}

void atras(int a){
  uint8_t i;

  Serial.println("Atrás...");

  motorDerecho.run(BACKWARD);
  motorIzquierdo.run(BACKWARD);
  for (i=0; i < 255; i++){
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }
  delay(500*a);
}

void derecha(int a){
  uint8_t i;

  Serial.println("Derecha...");
  
  motorDerecho.run(BACKWARD);
  motorIzquierdo.run(FORWARD);
  for (i=0; i < 255; i++){
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }
  delay(a * 100);
}

void izquierda(int a){
  uint8_t i;

  Serial.println("Izquierda...");

  motorDerecho.run(FORWARD);
  motorIzquierdo.run(BACKWARD);
  for (i=0; i < 255; i++){
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }
  delay(a * 100);
}
```
