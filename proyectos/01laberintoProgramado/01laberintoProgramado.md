# Laberinto programado

Para resolver un laberinto con tu carro mulltifuncional sin utilizar sensores puede escribir un programa que le indique que ruta tomar.

# ¿Cómo se usa?

Dibuja en el piso un carril para que tu carro lo recorra, puuedes utilizar cinta, bufandas, tu mochila o cualquier objeto que se te ocurra
ello.

Una vez definido el trayecto escribe un programa que le diga a tu carro que debe hacer:
* Si debe avanzar
* Si debe retroceder
* Si debe girar a la derecha o
* Si debe girar a la izquierda

Cada función recibe un valor que le indica cuantos bloques debe avanzar (1, 2, 3)  los que necesites para alcanzar la meta.

# Código

Copia el siguiente código en un Sketch de Arduino y bájalo  a tu carro.

Agrega las funciones que requieras.

```
/*
 *
 * Carro Multifuncional Laberinto Programado
 *
 * Ahora puedes programar tu auto para andar por un labrintoo tilizando
 * las siguientes funciones
 *
 * avanza(PASOS);
 * retrocede(PASOS);
 * derecha(PASOS);
 * izquierda(PASOS);
 * detener(PASOS);
 *
 * PASOS es un número entero, 0, 1, 2, 3.
 
 *  
 *  Si tienes sugerencias o aportes puedes hacerlos en 
 *  gitlab.com/dpw19/carroMultifuncional
 *  
 *  Author: Pavel E. Vázquez Mtz. pavel_e@riseup.net
 *  
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *    
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

#include <AFMotor.h>

AF_DCMotor motorDerecho(1);
AF_DCMotor motorIzquierdo(2);

void setup() {
  Serial.begin(19200);
  Serial.println("Carro Multifuncional");
}

void loop() {
  uint8_t i;
  
// adelante(3);
// atras(3);
// derecha(20);
// izquierda(20);
// detener(2);

  Serial.println("Hecho!!!");

  while(1);
}


void adelante(int a){
  uint8_t i;

  Serial.println("Adelante...");

  motorDerecho.run(FORWARD);
  motorIzquierdo.run(FORWARD);

  for (i=0; i < 255; i++) {
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }
  delay(500*a);
}

void detener(int a){
  uint8_t i;

  Serial.println("Detener...");

  for (i=255; i != 0 ; i--) {
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);  
    delay(5);
  }
  motorDerecho.run(RELEASE);
  motorIzquierdo.run(RELEASE);
  delay(500*a);
}

void atras(int a){
  uint8_t i;

  Serial.println("Atrás...");

  motorDerecho.run(BACKWARD);
  motorIzquierdo.run(BACKWARD);
  for (i=0; i < 255; i++){
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }
  delay(500*a);
}

void derecha(int a){
  uint8_t i;

  Serial.println("Derecha...");
  
  motorDerecho.run(BACKWARD);
  motorIzquierdo.run(FORWARD);
  for (i=0; i < 255; i++){
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }
  delay(a * 100);
}

void izquierda(int a){
  uint8_t i;

  Serial.println("Izquierda...");

  motorDerecho.run(FORWARD);
  motorIzquierdo.run(BACKWARD);
  for (i=0; i < 255; i++){
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }
  delay(a * 100);
}



```
