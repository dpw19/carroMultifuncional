# Seguidor de luz

```

/*
 *
 * Carro Multifuncional Sigue la Luz
 *
 * Para que tu auto siga la luz debes agregar el sensor LDR
 * 
 * Verifica la conexion en 
 * gitlab.com/dpw19/carroMultifuncional/
 *   
 * Si tienes sugerencias o aportes puedes hacerlos en 
 * gitlab.com/dpw19/carroMultifuncional
 *  
 * Author: Pavel E. Vázquez Mtz. pavel_e@riseup.net
 *  
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *    
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *    
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

#include <AFMotor.h>

int PIN_LDR1 = A0;
int PIN_LDR2 = A1;

int VALOR_LDR1 = 0;
int VALOR_LDR2 = 0;

int VALOR_LDR1_MAX = 0;
int VALOR_LDR2_MAX = 0;

AF_DCMotor motorDerecho(1);
AF_DCMotor motorIzquierdo(2);

void setup() {
  Serial.begin(19200);
  Serial.println("Carro Multifuncional... Sigue la luz");
  
  Serial.print("Muestreando... ");
  for(int i = 0; i <= 100; i++){
    VALOR_LDR1 = analogRead(PIN_LDR1);
    if(VALOR_LDR1 > VALOR_LDR1_MAX)
      VALOR_LDR1_MAX = VALOR_LDR1;
    VALOR_LDR2 = analogRead(PIN_LDR2);
    if(VALOR_LDR2 > VALOR_LDR2_MAX)
      VALOR_LDR2_MAX = VALOR_LDR2;
  }
  
  Serial.println("OK");
  Serial.println ("Valor Máximo");
  Serial.print(VALOR_LDR1_MAX);
  Serial.print("\t");
  Serial.println(VALOR_LDR2_MAX);
}

void loop() {
  uint8_t i;
  int x, y;
  
  VALOR_LDR1 = analogRead(PIN_LDR1);
  VALOR_LDR2 = analogRead(PIN_LDR2);
    
  x = map(VALOR_LDR1, 0, VALOR_LDR1_MAX, 0, 100);
  y = map(VALOR_LDR2, 0, VALOR_LDR2_MAX, 0, 100); 

  Serial.print(x);
  Serial.print("\t");
  Serial.println(y);

  if (x >= 120 && y >= 120)
    atras();
  else if(x > 115 && y > 110)
    derecha(1);
  else if(x > 110 && y > 115)
    izquierda(1);   
  else
    detener();

  delay(100);
}


void adelante(){
  uint8_t i;

  Serial.println("Adelante...");

  motorDerecho.run(FORWARD);
  motorIzquierdo.run(FORWARD);

  for (i=0; i < 255; i++) {
    motorDerecho.setSpeed(i);
    motorIzquierdo.setSpeed(i);
    delay(5);
  }

}

void detener(){
  uint8_t i;

  Serial.println("Detener...");

//  for (i=255; i != 0 ; i--) {
//    motorDerecho.setSpeed(i);
//    motorIzquierdo.setSpeed(i);  
//    delay(5);
//  }
  motorDerecho.run(RELEASE);
  motorIzquierdo.run(RELEASE);
}

void atras(){
  uint8_t i;

  Serial.println("Atrás...");

  motorDerecho.run(BACKWARD);
  motorIzquierdo.run(BACKWARD);
  motorDerecho.setSpeed(254);
  motorIzquierdo.setSpeed(254);
//  for (i=0; i < 255; i++){
//    motorDerecho.setSpeed(i);
//    motorIzquierdo.setSpeed(i);
//    delay(5);
//  }
}

void derecha(int a){
  uint8_t i;

  Serial.println("Derecha...");
  
  motorDerecho.run(BACKWARD);
  motorIzquierdo.run(FORWARD);
  motorDerecho.setSpeed(254);
  motorIzquierdo.setSpeed(254);
  delay(a*10);
}

void izquierda(int a){
  uint8_t i;

  Serial.println("Izquierda...");

  motorDerecho.run(FORWARD);
  motorIzquierdo.run(BACKWARD);
  motorDerecho.setSpeed(254);
  motorIzquierdo.setSpeed(254);

  
  delay(a*10);
}


```